import os
import matplotlib.pyplot as plt
import numpy as np
from dynamiks.views import XYView

# For the site
from dynamiks.sites import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField

# For flow simulation
from dynamiks.dwm import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator
from dynamiks.dwm.particle_motion_models import HillVortexParticleMotion

# Load the v80 wind turbine, which will be used later
from py_wake.examples.data.hornsrev1 import V80
from dynamiks.wind_turbines import PyWakeWindTurbines
from dynamiks.wind_turbines.hawc2_windturbine import HAWC2WindTurbines
from py_wake.examples.data.dtu10mw._dtu10mw import DTU10MW
from dynamiks.sites._site import UniformSite


if __name__ == '__main__':

    case = 'example4'
    # First we setup the site object.
    ws = 10     # mean wind speed
    ti = 0.1   # turbulence intensity

    # Creating the turbulence field, and then the site.
    tf = MannTurbulenceField.from_netcdf(filename='my_turb.nc')

    # Scale your turbulence box, see explanation and addtional arguments at
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-intensity
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-scaling
    tf.scale_TI(TI=ti, U=ws)

    site = TurbulenceFieldSite(ws=ws, turbulenceField=tf)
    # site = UniformSite(ws=ws, ti=ti)  # alternative site with uniform non-turbulent inflow

    # Set up the wind turbines
    if 1:
        # PyWake wind turbines
        wts = PyWakeWindTurbines(x=[0, 1000], y=[0, 0], types=0,
                                 windTurbine=DTU10MW())
    else:
        # HAWC2 wind turbines
        wts = HAWC2WindTurbines(
            x=[0, 800],
            y=[0, 0],
            htc_filename_lst=["./DTU10MW/htc/DTU_10MW_RWT_yaw.htc"],
            case_name=case,
            types=[0],
            suppress_output=True,
        )
        wts.add_sensor('yaw', lambda wt: wt.yaw_tilt()[0],
                       setter=lambda wt, value: wt.h2.set_variable_sensor_value(1, (-np.deg2rad(value)).tolist()))
    os.makedirs(case, exist_ok=True)

    # Then we setup the flow simulation object.
    fs = DWMFlowSimulation(site=site,
                           windTurbines=wts,
                           particleDeficitGenerator=jDWMAinslieGenerator(),
                           particleMotionModel=HillVortexParticleMotion(),
                           # Note that you need the Hill model for adding the wake deflection.
                           dt=1,          # time step [s]
                           d_particle=.2,   # distance between particles, normalized with windturbine diameter
                           # addedTurbulenceModel=None,  # use this to run with UniformSite
                           )

    # set the yaw misaligment of the upstream turbine to 5 degrees
    wts.sensors.yaw = [5, 0]
    fs.run(150, verbose=1)

    # How long should the animaton be, before you can see the wake deflection?
    fig = plt.figure(figsize=(10, 4))
    view = XYView(z=119, x=np.linspace(-200, 2000, 500), y=np.linspace(-400, 400))
    fs.animate(200, view=view, filename=f"{case}/Flowfield_{wts.__class__.__name__}.gif", interval=.5)
