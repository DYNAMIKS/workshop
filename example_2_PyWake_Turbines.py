"""
In this example you will setup a small windfarm using DTU10MW turbines from PyWake.
"""
import os
import matplotlib.pyplot as plt
import numpy as np

from dynamiks.views import XYView
from dynamiks.visualizers import ParticleVisualizer

# For the site
from dynamiks.sites import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField

# For flow simulation
from dynamiks.dwm import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator

from py_wake.examples.data.dtu10mw._dtu10mw import DTU10MW
from py_wake.utils.plotting import setup_plot
from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines


if __name__ == '__main__':

    # First you need to set up the turbulence field and the site object.

    # Start by defining a mean wind speed and a turbulence intensity.
    ws = 10     # mean wind speed
    ti = 0.08   # turbulence intensity
    folder = 'example2'
    os.makedirs(folder, exist_ok=True)

    # Creating the turbulence field, and then the site.
    tf = MannTurbulenceField.from_netcdf(filename="my_turb.nc")

    # Scale your turbulence box, see explanation and addtional arguments at
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-intensity
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-scaling
    tf.scale_TI(TI=ti, U=ws)

    site = TurbulenceFieldSite(ws=ws, turbulenceField=tf)

    # Set up the wind turbines
    wts = PyWakeWindTurbines(x=[0, 800], y=[0, 0],
                             windTurbine=DTU10MW())

    fs = DWMFlowSimulation(site=site,
                           windTurbines=wts,
                           particleDeficitGenerator=jDWMAinslieGenerator(),
                           dt=1,  # time step [s]
                           d_particle=.2,  # distance between particles, normalized with wind turbine diameter
                           )

    # Run and plot the simulation
    fig = plt.figure(figsize=(10, 4))
    fs.run(50, verbose=1)
    view = XYView(z=119, x=np.linspace(-200, 1000, 500), y=np.linspace(-200, 200), visualizers=[ParticleVisualizer()])
    fs.animate(fs.time + 50, view=view, filename=f"{folder}/Flowfield.gif", interval=0.5)

    da = wts.sensors.to_xarray()
    for sensor in da.sensor:
        plt.figure(figsize=(6, 3))
        for wt in da.wt:
            da.sel(sensor=sensor, wt=wt).plot(label=f'wt {wt.item()}')
        setup_plot(title=sensor.item(), xlabel='Time [s]')
        plt.savefig(f'{folder}/{sensor.item()}.png')
