import os
import time
import glob
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from dynamiks.views import XYView
from dynamiks.sites import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField
from dynamiks.dwm import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator
from dynamiks.dwm.particles_model import WindTurbinesParticles, DistributedWindTurbinesParticles
from dynamiks.wind_turbines import PyWakeWindTurbines
from dynamiks.wind_turbines.hawc2_windturbine import HAWC2WindTurbines
from dynamiks.visualizers._visualizers import ParticleVisualizer
from dynamiks.utils.data_dumper import DataDumper
from py_wake.utils import layouts
from py_wake.examples.data.dtu10mw._dtu10mw import DTU10MW



os.environ['OPENBLAS_NUM_THREADS'] = '1' # needed for >32 turbines
if __name__ == '__main__':

    case = 'example5'
    # First we setup the site object.
    ws = 10     # mean wind speed
    ti = 0.1   # turbulence intensity

    # Creating the turbulence field, and then the site.
    tf = MannTurbulenceField.from_netcdf(filename='my_turb.nc')

    # Scale your turbulence box, see explanation and addtional arguments at
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-intensity
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-scaling
    tf.scale_TI(TI=ti, U=ws)

    site = TurbulenceFieldSite(ws=ws, turbulenceField=tf)

    N = 32
    D = 178
    wt_x, wt_y = layouts.rectangle(N, int(np.sqrt(N)), 1.5 * D)

    # Set up the wind turbines
    t = time.time()
    if 1:
        windTurbinesParticles = WindTurbinesParticles
    else:
        windTurbinesParticles = DistributedWindTurbinesParticles

    if 1:
        # PyWake wind turbines
        wts = PyWakeWindTurbines(x=wt_x, y=wt_y, windTurbine=DTU10MW())
    else:
        # HAWC2 wind turbines
        wts = HAWC2WindTurbines(
            x=wt_x, y=wt_y, htc_filename_lst=["./DTU10MW/htc/DTU_10MW_RWT.htc"],
            types=0, case_name=case,
            suppress_output=True,  # set to True to hide output from HAWC2
        )
    print(f"Wind turbines setup in {time.time()-t:.0f}s")
    os.makedirs(case, exist_ok=True)
    id = f'{windTurbinesParticles.__name__}_{N}{wts.__class__.__name__}'
    
    time_last = time.time()
    def step_time(fs):
        global time_last
        ts = time.time() - time_last
        time_last = time.time()
        return ts
    step_time_dumper = DataDumper(step_time)

    # Then we setup the flow simulation object.
    fs = DWMFlowSimulation(site=site,
                           windTurbines=wts,
                           particleDeficitGenerator=jDWMAinslieGenerator(),
                           # Note that you need the Hill model for adding the wake deflection.
                           dt=1,          # time step [s]
                           d_particle=.5,   # distance between particles, normalized with windturbine diameter
                           windTurbinesParticles=windTurbinesParticles,
                           step_handlers=[step_time_dumper]
                           )

    plt.figure(figsize=(12, 12))
    view = XYView(z=119, x=np.linspace(wt_x.min() - 200, wt_x.max() + 200, 200),
                  y=np.linspace(wt_y.min() - 200, wt_y.max() + 200, 200), ax=plt.gca(),
                  visualizers=[ParticleVisualizer()])
    fs.show(view=view, block=False)
    plt.savefig(f"{case}/Flowfield_{id}_t{fs.time}s.png")

    fs.run(50, verbose=1)

    plt.cla()
    fs.show(view=view, block=False)
    plt.savefig(f"{case}/Flowfield_{id}_t{fs.time}s.png")
    step_time_dumper.to_xarray().to_netcdf(f"{case}/step_time_{id}.nc")

    plt.figure()
    for fn in glob.glob(f'{case}/step_time_*.nc'):
        xr.load_dataarray(fn).plot(label=fn.replace(f'{case}/step_time_', '')[:-3])
    plt.legend()
    plt.show()
    plt.savefig(f"{case}/step_time.png")

