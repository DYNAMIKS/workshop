import pytest
import glob
import os

@pytest.mark.parametrize('i', [1,2,3,4,5,6])
def test_examples(i):
    if i==6:
        try:
            import mpi4py
        except ModuleNotFoundError:
            pytest.xfail('mpi4py not available')
                
    f = glob.glob(f'example_{i}_*.py')[0]
    assert os.system(f'python {f}')==0

