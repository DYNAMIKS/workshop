# Installation issues



### Installation fails due to pip not being able to build PyWake

DYNAMIKS installs PyWake from its git repo. On Windows, this process might fail because the path to some of its files is too long. In the log you will read something like `... py_wake/tests/test_files/fuga/LUTs_Zeta0=0.00_16_32_D120_zhub90_zi400_z0=0.00001000_z29.6-207.9_UL_nx512_ny128_dx30.0_dy7.5.nc does not exist`. 

**Solution:** Enable the support for long file paths by following the steps [in this guide](https://www.thewindowsclub.com/how-to-enable-or-disable-win32-long-paths-in-windows-11-10) or run the following command in powershell:

```
Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem -Name LongPathsEnabled -Value 1
```

