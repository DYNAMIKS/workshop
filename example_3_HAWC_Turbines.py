"""
In this example you will setup a small windfarm using DTU10MW HAWC2 wind turbines
This will also show how you can add sensors to your turbines, and how you can extract data from these sensors.
"""
import os
import matplotlib.pyplot as plt
import numpy as np

from dynamiks.views import XYView

# For the site
from dynamiks.sites import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField

# For flow simulation
from dynamiks.dwm import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator

from dynamiks.wind_turbines.hawc2_windturbine import HAWC2WindTurbines
from py_wake.utils.plotting import setup_plot


if __name__ == '__main__':

    # First we setup the site object.
    ws = 10     # mean wind speed
    ti = 0.08   # turbulence intensity
    case = 'example3'
    os.makedirs(case, exist_ok=True)

    # Creating the turbulence field, and then the site.
    tf = MannTurbulenceField.from_netcdf(filename='my_turb.nc')

    # Scale your turbulence box, see explanation and addtional arguments at
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-intensity
    # https://hipersim.pages.windenergy.dtu.dk/hipersim/MannTurbulenceField.html#Turbulence-scaling
    tf.scale_TI(TI=ti, U=ws)

    site = TurbulenceFieldSite(ws=ws, turbulenceField=tf)

    # Set up the wind turbines
    wts = HAWC2WindTurbines(
        x=[0, 800],
        y=[0, 150],
        htc_filename_lst=["./DTU10MW/htc/DTU_10MW_RWT.htc"],
        case_name=case,
        types=[0],
        suppress_output=False,  # set to True to hide output from HAWC2
    )

    # Here we define some sensors that is accessible directly in Dynamiks
    wts.add_sensor("rotor", "constraint bearing1 shaft_rot 2", ext_lst=['angle', 'speed'])
    wts.add_sensor("pitch", "constraint bearing2 pitch1 5", ext_lst=['angle', 'vel'])

    # Then we setup the flow simulation object.
    fs = DWMFlowSimulation(site=site,
                           windTurbines=wts,
                           particleDeficitGenerator=jDWMAinslieGenerator(),
                           dt=1,          # time step [s]
                           d_particle=.2,   # distance between particles, normalized with wind turbine diameter
                           )

    # Run and plot the simulation for 5 seconds.
    fig = plt.figure(figsize=(10, 4))
    view = XYView(z=119, x=np.linspace(-200, 1200), y=np.linspace(-200, 500))
    fs.animate(5, view=view, filename=f"{case}/Flowfield.gif", interval=1)

    fs.run(10, verbose=1)

    # This then extracts the sensor data from the simulation to a xarray dataarray.
    sim_res = wts.sensors.to_xarray()

    # Plot and save the results
    for sensor in sim_res.sensor:
        plt.figure()
        for wt in sim_res.wt:
            sim_res.sel(wt=wt, sensor=sensor).plot(label="Turbine: " + str(wt.item()))
        setup_plot(title=f"Dynamiks {sensor.item()}", xlabel="Time [s]")
        plt.savefig(f"{case}/Dynamiks_{sensor.item()}.png")

    # load and plot sensors from HAWC2 result files
    wts.h2.close() # save buffered sensor values to result file
    from wetb.gtsdf import gtsdf
    for n in [0, 1, 10, 13, 27]:
        plt.figure()
        for i in sim_res.wt.values:
            htc = wts.htc_lst[i]
            f = htc.output.filename.values[0] + ".hdf5"
            time, data, info = gtsdf.load(os.path.join(htc.modelpath, f))
            plt.plot(time, data[:, n], label=f"WT{i}")
        sensor = info['attribute_names'][n].replace(":", "")
        setup_plot(title=f"HAWC2, {sensor}", xlabel="Time [s]")
        plt.savefig(f"{case}/HAWC2_{sensor}.png")
        plt.close()
