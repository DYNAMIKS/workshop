# Important notes!!!
- Follow the instructions carefully, step by step (e.g. installing dynamiks before starting the interactive job may cause high workload on the login node)
- Simulations with more than a few HAWC2 wind turbines should be run from the burst buffer, e.g. `/work/users/<username>` and remember to remove all files afterwards

See potential problem if you don't

# Install on Sophia

1. Log into sophia
2. Start interactive job
```
srun --partition workq --nodes 1 --pty bash
```
3. Load git, python and intel (needed for MPI)
```
ml purge
ml git Python/3.11.2-GCCcore-12.2.0-bare intel/19.0.4.243
source /apps/external/hpcx/2.5.0/MLNX_OFED_LINUX-4.6-1.0.1.1-redhat7.6-x86_64/ompi-mt-intel-19.0.4.243.sh && hpcx_load
```
4. Make and activate virtual environment 
```
python -m venv dynamiks_workshop_venv --clear
source dynamiks_workshop_venv/bin/activate
```
5. Clone workshop files
```
git clone https://gitlab.windenergy.dtu.dk/DYNAMIKS/workshop.git
```
6. Install dynamiks and dependencies
```
cd workshop
pip install "dynamiks[hawc2,mpi] @ git+https://gitlab.windenergy.dtu.dk/DYNAMIKS/dynamiks.git"
```
To run example 1, jump to 5. below

# Run on Sophia

1. Log into sophia
2. Load git, python and intel (needed for MPI)
```
ml git Python/3.11.2-GCCcore-12.2.0-bare intel/19.0.4.243
```
3. Activate environment
```
source dynamiks_workshop_venv/bin/activate
```
4. Start interactive job
```
srun --partition workq --nodes 1 --pty bash
```
5. run example 1
```
cd workshop
python example_1_turbbox.py
```

# Potential problems

## Examples with HAWC2 turbines crashes (license issue)
Error message:
```
Verifying using HAWCLicense...
DLL does not exist: workshop/DTU10MW/HAWC2License.so
...
Exception: H2LibThread process died before or while executing read_input(...)
```
**Solution:** Install the provided `license.cfg` file by:
```
/groups/hawc/binaries/license_manager install hawc2 license.cfg
```

## Examples with HAWC2 turbines crashes (mpi issue)
**Error message:**
```
...  
  File "dynamiks_workshop_venv/lib/python3.11/site-packages/multiclass_interface/mpi_interface.py", line 18, in <module>
    from mpi4py import MPI
ModuleNotFoundError: No module named 'mpi4py'
```
or 
```
  File "dynamiks_workshop_venv/lib/python3.11/site-packages/multiclass_interface/mpi_interface.py", line 18, in <module>
    from mpi4py import MPI
ImportError: libimf.so: cannot open shared object file: No such file or directory
```
**Cause:** `h2lib` assumes you run with MPI

**Solution:** Omit the argument `--ntasks-per-node=32` when starting the interactive job


## MPI import fails

**Error message:**
```
Abort(1094799) on node 0 (rank 0 in comm 0): Fatal error in PMPI_Init_thread: Other MPI error, error stack:
MPIR_Init_thread(666)......:
MPID_Init(922).............:
MPIDI_NM_mpi_init_hook(719): OFI addrinfo() failed (ofi_init.h:719:MPIDI_NM_mpi_init_hook:No data available)
```

**Cause:**
`mpi4py` was not build correctly

**Solution:**
```bash
ml purge
ml git Python/3.11.2-GCCcore-12.2.0-bare intel/19.0.4.243
source /apps/external/hpcx/2.5.0/MLNX_OFED_LINUX-4.6-1.0.1.1-redhat7.6-x86_64/ompi-mt-intel-19.0.4.243.sh && hpcx_load
source dynamiks_workshop_venv/bin/activate
pip install mpi4py --no-cache-dir --force-reinstall
```