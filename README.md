# Workshop instructions


## Installation

Instructions for Sophia is found [here](install_sophia.md)

**clone workshop files**

```
git clone https://gitlab.windenergy.dtu.dk/DYNAMIKS/workshop.git
```

**Python**

Version must be 3.8-3.11 

Download and install python or use an existing installation. 


**Make a python environment and activate**

```
python -m venv dynamiks_workshop_venv --clear

# windows
dynamiks_workshop_venv\scripts\activate 

# linux
source dynamiks_workshop_venv/bin/activate
```

**Install dynamiks and HAWC2Lib**
```
pip install "dynamiks[hawc2] @ git+https://gitlab.windenergy.dtu.dk/DYNAMIKS/dynamiks.git"
```

**Run examples**

```python example_1_turbbox.py```


## Example 1 - Turbulence field

Example 1 will create a turbulence field, save it to my_turb.nc and save a contour plot of the bottom plane to my_turb.png

1. Run example 1
2. check my_turb_xy.png
3. make a plot of the back plane (x=0). Hint: use `.plot(y='z')`, to get z on the vertical axis
4. What is the physical size of turbulence field
5. What should Nx, Ny, Nz be for a one hour simulation of a wind farm  with 2x2 DTU10MW wind turbines separated by 5D (D=178m, hub height=120m) in 12 m/s and 3m spacing between grid points in all three directions


## Example 2 - Two PyWake Turbines

Example 2 will run a 100s Dynamiks simulation with two PyWake wind turbines. It will save an animation and some wind turbine sensor plots

1. Run example 2
2. Check the animation and plots
3. Add an additional DTU10MW wind turbine at position (400,200) and rerun
4. Extend the animation view to include all wind turbines and rerun.
5. rename the folder, change the wind direction to 310deg (see https://dynamiks.pages.windenergy.dtu.dk/dynamiks/notebooks/DWMFlowSimulation.html#Wind-direction), rerun and compare the wind speed



## Example 3 - Two HAWC2 Turbines

Example 3 will run a Dynamiks simulation with two DTU10MW HAWC2 wind turbines, save an animation of the first 5s and some sensor plots of the first 10s

1. Run example 3
2. Check the animation and the plots.
3. You can disable the HAWC2 output by setting `suppress_output=True`. Alternatively, you can specify a `logfile` command in the htc file
4. Compare "Dynamiks_wind_speed_0.png" and "HAWC2_WSP gl. coo.,Vy.png". What are the differences? 
Hint: Compare plots or insert code below in the end of the script. The Dynamiks wind speed sensor is added in the `FreeWindCoupling.initialize` in [hawc2_windturbine.py](https://gitlab.windenergy.dtu.dk/DYNAMIKS/dynamiks/-/blob/main/dynamiks/wind_turbines/hawc2_windturbine.py) while the HAWC2 sensor is added in the [htc file](https://gitlab.windenergy.dtu.dk/DYNAMIKS/workshop/-/blob/main/DTU10MW/htc/DTU_10MW_RWT.htc?ref_type=heads#L614)
5. Extend the simulation time (or animation time) to 150s, rerun and check the plots, especially the blade-root flap-wise bending moment, 'HAWC2_Mx coo blade1.png'
```
    plt.figure()
    sensor = 'wind_speed_u'
    n = 13
    for wt in sim_res.wt.values:
        htc = wts.htc_lst[wt]
        f = htc.output.filename.values[0] + ".hdf5"
        time, data, info = gtsdf.load(os.path.join(htc.modelpath, f))
        c = plt.plot(time, data[:, n], label=f"HAWC2, WT{wt}")[0].get_color()
        sim_res.sel(wt=wt, sensor=sensor).plot(label=f"Dynamiks, WT{wt}", ls='--', color=c)
    setup_plot(title=f"HAWC2, {sensor}", xlabel="Time [s]")
    plt.savefig(f"{case}/compare.png")
```


## Example 4 - Two Turbines with yaw misalignment

Example 4 will run a Dynamiks simulation with two DTU10MW wind turbines and save an animation of the 150s-200s.

The ParticleMotionModel is set to HillVortexParticleMotion, which supports wake deflection and the yaw misalignment of the upstream turbine is 5 deg.

An `if 1:` block allows to switch between PyWake and HAWC2 turbines.

1. Run example 4
2. Check the animation
3. Increase the yaw misalignment of the upstream turbine to 20 deg, rerun and check the animation
4. Decrease the turbulence intensity to 4%, rerun and check the animation
3. Switch to HAWC2 turbines, change the script to animate from the beginning and rerun. In the meantime, you can compare the HAWC2 model (htc file) with the model from the previous example. The `general variable` sensor is documented in the [HAWC2 manual](https://hawc2.pages.windenergy.dtu.dk/documentation/hawc2-manual/16-output.html#general-general-output-commands)
4. Check the animation and see what happens to the upstream turbine during the first 20s
5. Check the hawc2 result files and compare e.g. `Mx coo: blade1` in [Pdap](https://tools.windenergy.dtu.dk/home/Pdap/)

## Example 5 - Wind Farm

Example 5 will run a Dynamiks simulation with 32 DTU10MW turbines. It will simulate 50s and save a snapshot of the initial and final flow field. Furthermore, it will save the iteration time as a netcdf file and make a plot of iteration times of the performed runs.

Follow the [instructions for Sophia](install_sophia.md) to install Dynamiks on Sophia.

Start an interactive job on an **exclusive** node on sophia before running the example

```
srun --partition workq --nodes 1 --pty --exclusive bash
```

1. If you have not run example 1, then run it to obtain the turbulence file 'my_turb.nc'
2. Run example 5
3. Check the flow field plots. What is the size of the farm compared to the turbulence field and how is that handled?
3. Check the step_time plot. 
  - Why are some iterations slower than others
  - Why are the iterations slower after 30s
3. Switch to `DistributedWindTurbinesParticles` via the `if 1:` line, rerun and compare the step time
3. Decrease the simulation time from 50 to 20s, switch to `HAWC2WindTurbines`, rerun and compare the step time. How often are the iteration time considerably longer and why?
4. Increase the number of wind turbines, `N` to 64, rerun and compare the step time

## Example 6 - Larger Wind Farm (MPI)

Example 6 will run a Dynamiks simulation with 32 DTU10MW HAWC2 turbines and distributed particles using MPI. It will simulate 20s and save a snapshot of the initial and final flow field. Furthermore, it will save the iteration time as a netcdf file and make a plot of iteration times of the performed runs and some runs from example 5.

1. Before running with MPI you must exit the interactive node 
2. Run example 6 with MPI

```
srun --partition workq,windq --mpi=pmix_v2 --nodes 1 --ntasks 32 --exclusive --unbuffered python example_6_Larger_Wind_Farm_mpi.py
```

2. Increase the number of wind turbines, `N` to 64 and run again with 64 mpi workers on two compute nodes:
```
srun --partition workq,windq --mpi=pmix_v2 --nodes 2 --ntasks 64 --exclusive --unbuffered python example_6_Larger_Wind_Farm_mpi.py
```

3. Check the step time plot



